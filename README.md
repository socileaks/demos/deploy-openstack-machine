# Deploy OpenStack Machine

 - This Ansible Playbook aims to create an OpenStack instance.

 - It has been tested successfully in a private cloud.

 - It is parameterized for the ~okeanos cloud.

 - But does not work, as they don't support the `openstack image list`
   command.
